# hep-ml website

## Contributing guide

### Adding to a page:
To add to an existing page on the website, go to the page you are interested in on the gitlab.com repository (probably in [`/content/page`](content/page)), and click the pencil-like edit button on top of the page. This will open the page in an editor, and will make a PR for you as well. You can also use standard git procedures, of course.


### Adding a page:

You'll need to add a new page similar to the existing ones, and add a link in the [`config.toml`](config.toml) file.

### Adding a news story:

Add a post in [`/content/post`](content/post), and make sure you include the date.


