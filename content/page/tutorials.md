---
title: "Tutorials"
date: 2018-04-26T17:04:03-04:00
---


# General

* [PyTorch example tutorial][pytorchexamp]: A great tutorial through the computational steps to ML, using a fully-connected ReLU network. This is a very accessible tutorial even if you are not interested in PyTorch (the first example is pure NumPy).

[pytorchexamp]: http://pytorch.org/tutorials/beginner/pytorch_with_examples.html
